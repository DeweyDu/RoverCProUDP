#include "Connect.h"
#define EEPROM_SIZE 64

extern const unsigned char connect_on[800];
extern const unsigned char connect_off[800];

#define SYSNUM     3

uint64_t realTime[4], time_count = 0;
bool k_ready = false;
uint32_t key_count = 0;

IPAddress local_IP(192, 168, 4, 100 + SYSNUM );
IPAddress gateway(192, 168, 4, 1);
IPAddress subnet(255, 255, 0, 0);
IPAddress primaryDNS(8, 8, 8, 8); //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional

const char *ssid = "SXRobot20robot";
const char *password = "eLY5QJAX4gHqb5cp";

WiFiUDP Udp;
uint32_t send_count = 0;
uint8_t system_state = 0;
int count = 0;


void SendUDP(uint8_t buffer[])
{
  if ( WiFi.status() == WL_CONNECTED )
  {
    Udp.beginPacket(IPAddress(192, 168, 4, 1), 1000 + SYSNUM );
    Udp.write(buffer, 8);
    Udp.endPacket();
  } else {
    count ++;
    if ( count > 500 )
    {
      WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS);
      count = 0;
    }
  }
}

void InitConnect() {
  
  WiFi.mode(WIFI_STA);

  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS))
  {
    Serial.println("STA Failed to configure");
  }

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Udp.begin(2000);
}
