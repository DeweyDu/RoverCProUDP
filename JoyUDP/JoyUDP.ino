#include <M5StickCPlus.h>
#include "Connect.h"

uint8_t SendBuff[8] =   { 
  0xAB, // 校验，不要改
  0x00, // 第一舵机角度
  0x00, // 第二舵机角度
  0x00, // 左前轮
  0x05, // 右前轮
  0x00, // 左后轮
  0x00, // 右后轮
  0xAB // 校验，不要改
};

void setup() {
  // put your setup code here, to run once:
  M5.begin();
  Wire.begin(0, 26, 10000);
  
  InitConnect();
}

void loop() {
  SendUDP(SendBuff);
  delay(2000);
  M5.update();
}
