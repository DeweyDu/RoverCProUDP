#ifndef _CONNECT_H_
#define _CONNECT_H_
#include <WiFi.h>
#include <WiFiUdp.h>
#include "EEPROM.h"

void SendUDP(uint8_t buffer[]);

void InitConnect();

#endif
